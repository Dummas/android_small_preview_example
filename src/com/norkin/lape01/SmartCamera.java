package com.norkin.lape01;

import java.io.IOException;
import java.io.InputStream;
import java.util.Random;

import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.LocalServerSocket;
import android.net.LocalSocket;
import android.net.LocalSocketAddress;
import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.support.v4.app.NavUtils;

public class SmartCamera extends Activity implements SurfaceHolder.Callback {
	
	protected static final String TAG = "SmartCamera";
	/* Media Recorder */
	MediaRecorder recorder;
	/* Surface Holder */
	SurfaceHolder holder;
	/* Recording checker */
	boolean recording = false;
	/* Local Server Socket */
	private LocalServerSocket lss = null;
	/* Local Sockets */
	private LocalSocket receiver, sender = null;
	private static int id = 0;
	private int socketId;
	
	protected InputStream fis;
	
	public SmartCamera() {
		super();
		
		try {
			lss = new LocalServerSocket("com.lape.ol-" + id);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			finish();
		}
		
		socketId = id;
		id++;
	}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        recorder = new MediaRecorder();
        try {
			initSockets();
		} catch (IOException e) {
			e.printStackTrace();
			finish();
		}
        initRecorder();
        setContentView(R.layout.activity_smart_camera);
        SurfaceView cameraView = (SurfaceView) findViewById(R.id.surfaceView1);
//        SurfaceView cameraView = new SurfaceView(this);
        holder = cameraView.getHolder();
        holder.addCallback(this);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        
        try {
			fis = receiver.getInputStream();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        // Launch the thread
        launch_thread();
        
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_smart_camera, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    
    private void launch_thread() {
    	new Thread() {
    		@Override
    		public void run() {
    			
    			while (true) {
    				if (receiver.isConnected()) {
    					try {
							Log.d(TAG, "Available: " + fis.available());
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    				}
    			}
    		}
    	}.start();
    }
    
    private void initSockets() throws IOException {
    	receiver = new LocalSocket();
    	receiver.connect(new LocalSocketAddress("com.lape.ol-" + socketId ));
    	receiver.setReceiveBufferSize(500000);
    	receiver.setSendBufferSize(500000);
    	sender = lss.accept();
    	sender.setReceiveBufferSize(500000);
    	sender.setSendBufferSize(500000);
    }
    
    private void initRecorder() {
    	recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
    	recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);
    	
    	CamcorderProfile cpHigh = CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH);
    	recorder.setProfile(cpHigh);
    	recorder.setOutputFile(sender.getFileDescriptor());
    }
    
    private void prepareRecorder() {
    	recorder.setPreviewDisplay(holder.getSurface());
    	
    	try {
    		recorder.prepare();
    	} catch (IllegalStateException ex) {
    		ex.printStackTrace();
    		finish();
    	} catch (IOException e) {
			e.printStackTrace();
			finish();
		}
    }

	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}

	public void surfaceCreated(SurfaceHolder holder) {
		prepareRecorder();
		recorder.start();
		
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		if (recording) {
			recorder.stop();
			recording = false;
		}
		recorder.release();
		finish();
	}

}
